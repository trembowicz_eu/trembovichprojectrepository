import React from 'react'
import services from '../../services'
import { Link, useHistory } from 'react-router-dom';
import  {Routes} from '../../config.json'

const LoginForm = () => {
  const history = useHistory()
  const [userName, setUserName] = React.useState('Eugeny');
  const [password, setPassword] = React.useState('qazxswE1.');
  const [errors, setErrors] = React.useState({});

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    errors.UserName = null;
    errors.Password = null;
    errors.LoginFailed = null;
    
    if (!isValid()){
      return;
    }

    let isAuthentificationSuccessfull = await services.LoginService.login(userName, password);

    if(isAuthentificationSuccessfull){
      history.push(Routes.Board);
    }
    else{
      let errors = {};
      errors.LoginFailed = "User name or password invalid.";
      setErrors(errors);
    }
  }

  const isValid = () => {
    let isValid = true;
    let errors = {};

    if(userName.trim() === "") {
      errors.UserName = "User name is required.";
      setErrors(errors);
      isValid = false;
    }

    if(password.trim() === "") {
      errors.Password = "Password is required.";
      setErrors(errors);
      isValid = false;
    }

    return isValid;
  }

  return(
    <div>
      <div className="login-form">
        <h4>Please enter your details</h4>
          <form onSubmit={handleFormSubmit}>
            <div className="form-group">
              <label>User Name:</label>
              <input type="text" name="userName" onChange={e=>setUserName(e.target.value)} value={userName} className="form-control" />
              <span style={{ color: "red" }}>{errors.UserName}</span>
            </div>
            <div className="form-group">
              <label>Password:</label>
              <input type="password" name="password" onChange={e=>setPassword(e.target.value)} value={password} className="form-control" />
              <span style={{ color: "red" }}>{errors.Password}</span>
              <p className='text-danger'>{errors.LoginFailed}</p>
            </div>
            <div className="form-group">
              <button type="submit" className="btn btn-primary btn-lg btn-block">Login</button>
            </div>
          </form>
          <div className="text-center">
            Don't have an account? <Link to={Routes.SignUp}>Sign Up</Link>
          </div>
      </div>
    </div>
  )
}

export default LoginForm