import React from 'react';
import Layout from '../../layout';
import LoginForm from './form';

class LoginPage extends React.Component {
  render() {
    return(
      <Layout>
        <LoginForm />
      </Layout>
    )
  }
}

export default LoginPage