import React from "react";
import { Link } from 'react-router-dom';
import  {Routes} from '../../config.json'

const Filter = (props) => {
  const handleChange = (e) => {
    props.onChange(e.target.value);
  };

  return (
    <div className="col-lg-3">
      <h1 className="my-4">Filter</h1>
      <div className="form-group">
        <select
          className="form-control"
          value={props.selectedCategoryId}
          onChange={handleChange}
        >
          <option value="0">Category</option>
          {props.categories.map((category) => (
            <option key={category.id} value={category.id}>
              {category.name}
            </option>
          ))}
        </select>
      </div>
      <div className="form-group">
        <Link className="btn btn-primary btn-lg btn-block" to={Routes.Ad}>Сreate your ad</Link>
      </div>
    </div>
  );
};

export default Filter;