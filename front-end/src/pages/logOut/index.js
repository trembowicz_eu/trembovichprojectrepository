import React from 'react'
import { Redirect } from 'react-router-dom'

const SignOut = () => {
  window.localStorage.setItem('token', '')
}

const LogOutPage = () => {
  // React.useEffect(() => {
  //   SignOut()
  // }, [])
  SignOut()
  return <Redirect to="/" />
}

export default LogOutPage