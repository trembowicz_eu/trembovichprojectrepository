import React from 'react';
import Layout from '../../layout';
import AdForm from './form';

class AdPage extends React.Component {

  render() {
    return(
      <Layout>
        <AdForm />
      </Layout>
    )
  }
}

export default AdPage