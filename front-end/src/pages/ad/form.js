import React from 'react'
import services from '../../services' 
import CategoryDropDown from './category-dropdown'
import  {Routes} from '../../config.json'
import { useHistory } from 'react-router-dom';

const AdForm = () => {
  const history = useHistory()
  const [title, setTitle] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [category, setCategory] = React.useState();
  const [goodsLocation, setGoodsLocation] = React.useState('');
  const [price, setPrice] = React.useState('');
  const [contactNumber, setContactNumber] = React.useState('');
  const [image, setImage] = React.useState('');
  const [imageBase64, setImageBase64] = React.useState('');
  const [imageType, setImageType] = React.useState();
  const [errors, setErrors] = React.useState({});
  const reader = new FileReader();
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    errors.Title = null;
    errors.Description = null;
    errors.Category = null;
    errors.GoodsLocation = null;
    errors.Price = null;
    errors.ContactNumber = null;
    errors.Image = null;
    errors.CreateAdFailed = null;
    
    if (!isValid()){
      return;
    }
    
    let isCraeteAdSuccessfull = await services.AdServise.createAd(title, description, category, goodsLocation, price, contactNumber, imageBase64);

    if(isCraeteAdSuccessfull){
      history.push(Routes.Board);
    }
    else{
      let errors = {};
      errors.CreateAdFailed = "Something was wrong!";
      setErrors(errors);
    }
  }

  const isValid = () => {
    let isValid = true;
    let errors = {};

    if(title.trim() === "") {
      errors.Title = "Title is required.";
      setErrors(errors);
      isValid = false;
    }

    if(description.trim() === "") {
      errors.Description = "Description is required.";
      setErrors(errors);
      isValid = false;
    }
    
    if(category === undefined){
      errors.Category = "Category is required.";
      setErrors(errors);
      isValid = false;
    }

    if(goodsLocation.trim() === "") {
      errors.GoodsLocation = "Goods location is required.";
      setErrors(errors);
      isValid = false;
    }

    var priceRegex = /^\d+(\.\d{0,2})?$/g;
    if(price === '0' || price === '0.' || price ==='0.0' || price === '0.00' || price === '') {
      errors.Price = "Price is required.";
      setErrors(errors);
      isValid = false;
    } else if (!priceRegex.test(price)) {
      errors.Price = "Invalid format.";
      setErrors(errors);
      isValid = false;
    } 

    var contactNumberRegex = /^\+[0-9]{12}$/;
    if(contactNumber.trim() === "") {
      errors.ContactNumber = "Contact number is required.";
      setErrors(errors);
      isValid = false;
    } else if (!contactNumberRegex.test(contactNumber)) {
      errors.ContactNumber = "Invalid format. Allowed format +xxxxxxxxxxxx";
      setErrors(errors);
      isValid = false;
    }

    if(image.trim() === "") {
      errors.Image = "Photo is required.";
      setErrors(errors);
      isValid = false;
    } else if (!(imageType === "image/jpeg")) {
      errors.Image = "Uploaded file is not a valid image. Only JPG files are allowed.";
      setErrors(errors);
      isValid = false;
    }
    return isValid;
  }

  const uploadImage = (e) => {
    console.log(12);
    if(e.target.files[0] === undefined){
      setImageType(null);
    }else{
      setImageType(e.target.files[0].type)
    }
    setImage(e.target.value);
    reader.onload = function () {
      setImageBase64(reader.result);
    }; 
    reader.onerror = function (error) { 
      console.log('Error: ', error); 
    };

    if(e.target.files[0] === undefined){
      reader.readAsDataURL(new Blob());
    }else{
      reader.readAsDataURL(e.target.files[0]);
    }
  }

return(
  
  <div>
    <div className="col-lg-6">
      <h4>Creating an ad</h4>
        <form onSubmit={handleFormSubmit}>
          <div className="form-group">
            <label>Title:</label>
            <input type="text" name="title" onChange={e=>setTitle(e.target.value)} value={title} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Title}</span>
          </div>
          <div className="form-group">
            <label>Description:</label>
            <input type="text" name="description" onChange={e=>setDescription(e.target.value)} value={description} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Description}</span>
          </div>
          <div className="form-group">
            <CategoryDropDown onChange={e=>setCategory(e)} value={category} />
            <span style={{ color: "red" }}>{errors.Category}</span>
          </div>
          <div className="form-group">
            <label>Goods location:</label>
            <input type="text" name="goodsLocation" onChange={e=>setGoodsLocation(e.target.value)} value={goodsLocation} className="form-control"/>
            <span style={{ color: "red" }}>{errors.GoodsLocation}</span>
          </div>
          <div className="form-group">
            <label>Price:</label>
            <input type="text" name="price" onChange={e=>setPrice(e.target.value)} placeholder="0.00" step="0.01" min="0.00" value={price} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Price}</span>
          </div>
          <div className="form-group">
            <label>Contact number:</label>
            <input type="tel" name="contactNumber" onChange={e=>setContactNumber(e.target.value)} value={contactNumber} className="form-control"/>
            <span style={{ color: "red" }}>{errors.ContactNumber}</span>
          </div>
          <div className="form-group">
            <label>Photo:</label>
            <input type="file" name="imag" onChange={uploadImage} value={image} className="form-control" />
            <span style={{ color: "red" }}>{errors.Image}</span>
            <p className='text-danger'>{errors.CreateAdFailed}</p>
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary btn-lg btn-block">Create</button>
          </div>
        </form>
    </div>
  </div>
  )
}

export default AdForm