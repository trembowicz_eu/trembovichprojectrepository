import React from 'react'
import PropTypes from 'prop-types'
import  {BaseUri} from '../../config.json'

class CategoryDropDown extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      categories: [],
    }

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    fetch(`${BaseUri}/Category/get`, { 
      headers: new Headers({
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      })
    })
    .then(result => result.json())
    .then(result => this.setState({
      categories: result,
    }))
  }

  handleChange(e) {
    this.props.onChange(e.target.value);
  }

  render() {
    return(
        <div>
          <label>Category:</label>
          <select className="form-control" onChange={this.handleChange}>
            <option value="0">Choose category</option>
            {this.state.categories.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
          </select>
        </div>
    )
  }
}

CategoryDropDown.propTypes = {
  onChange: PropTypes.func.isRequired
}

export default CategoryDropDown
