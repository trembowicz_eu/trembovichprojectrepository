import React from 'react'
import services from '../../services'
import { useHistory } from 'react-router-dom';
import  {Routes} from '../../config.json'
import validator from 'validator'

const SignUpForm = () => {
  const history = useHistory()
  const [userName, setUserName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [errors, setErrors] = React.useState({});
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    errors.UserName = null;
    errors.Email = null;
    errors.Password = null;
    errors.userNameAlreadyExists = null;
    errors.emailAlreadyExists = null;


    if (!isValid()){
      return;
    }

    let isRegistrationSuccessfull = await services.SignupServise.signUp(userName, email, password);


    if(isRegistrationSuccessfull === 'registrationСompletedSuccessfully'){
      history.push(Routes.Login);
    } else{
      if(isRegistrationSuccessfull === 'userNameAlreadyExists'){
        let errors = {};
        errors.userNameAlreadyExists = "A user with the same name already exists!";
        setErrors(errors);
      }
      else{
        if(isRegistrationSuccessfull === 'emailAlreadyExists'){
          let errors = {};
          errors.emailAlreadyExists = "A user with the same email already exists!";
          setErrors(errors);
        }
      }
    }
  }


  const isValid = () => {
    let isValid = true;
    let errors = {};

    if(userName.trim() === "") {
      errors.UserName = "User name is required.";
      setErrors(errors);
      isValid = false;
    }

    if(email.trim() === "") {
      errors.Email = "Email is required.";
      setErrors(errors);
      isValid = false;
      }
      else{
        if(!(validator.isEmail(email))){
          errors.Email = "Enter valid Email!"
          setErrors(errors);
          isValid = false;
        }
      }

    if(password.trim() === "") {
      errors.Password = "Password is required.";
      setErrors(errors);
      isValid = false;
    }
    else{
      if(password.length < 8){
        errors.Password = "Should be at least 8 characters!"
        setErrors(errors);
        isValid = false;
      }
    }
    return isValid;
  }


return(
  <div>
    <div className="login-form">
      <h4>Sign Up</h4>
        <form onSubmit={handleFormSubmit}>
          <div className="form-group">
            <label>User Name:</label>
            <input type="text" name="userName" onChange={e=>setUserName(e.target.value)} value={userName} className="form-control"/>
            <span style={{ color: "red" }}>{errors.UserName}</span>
            <p className='text-danger'>{errors.userNameAlreadyExists}</p>
          </div>
          <div className="form-group">
            <label>Email:</label>
            <input type="text" size="30" name="email" onChange={e=>setEmail(e.target.value)} value={email} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Email}</span>
            <p className='text-danger'>{errors.emailAlreadyExists}</p>
          </div>
          <div className="form-group">
            <label>Password:</label> 
            <input type="password" name="password" onChange={e=>setPassword(e.target.value)} value={password} className="form-control" />
            <span style={{ color: "red" }}>{errors.Password}</span>
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary btn-lg btn-block">Sign Up</button>
          </div>
        </form>
    </div>
  </div>
  )
}

export default SignUpForm