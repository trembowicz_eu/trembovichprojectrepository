import React, { Component } from "react";
import Layout from "../../layout";
import SignUpForm from './form';

class SignUp extends Component {
  render() {
    return(
      <Layout>
        <SignUpForm />
      </Layout>
    )
  }
}

export default SignUp