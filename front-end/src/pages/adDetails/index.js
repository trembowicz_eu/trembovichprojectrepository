import React from "react";
import Layout from "../../layout";
import AdDetails from "./adDetails";
import { BaseUri } from "../../config.json";
import LoaderImage from "../../images/loader.svg";

class AdDetailsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      adDetails: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    fetch(`${BaseUri}/Ad/getAdById?adId=${this.props.id}`, {
      headers: new Headers({
        Authorization: "Bearer " + window.localStorage.getItem("token"),
      }),
    })
      .then((result) => result.json())
      .then((result) =>
        this.setState({
          adDetails: result,
          isLoading: false,
        })
      );
  }


  render() {
    window.scrollTo(0, 0);
    const { isLoading, adDetails } = this.state;
    return (
      <Layout>
        {!isLoading && (
          <AdDetails
            adDetails={adDetails}
            adId={this.props.id}
          />
        )}
        {isLoading && (
          <div className="loader">
            <img src={LoaderImage} alt="..." />
          </div>
        )}
      </Layout>
    );
  }
}

export default AdDetailsPage;
