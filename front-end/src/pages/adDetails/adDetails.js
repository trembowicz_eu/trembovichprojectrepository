import React from "react";
import { BaseUri } from "../../config.json";
import { useHistory } from "react-router-dom";

const AdDetails = (props) => {
  const history = useHistory();
  const { adDetails } = props;
  const { adId } = props;

  const handleDeleteClick = () => {
    fetch(`${BaseUri}/ad/delete?adId=${adId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + window.localStorage.getItem("token"),
      },
    });
    history.goBack();
  };

  const handleUpdateClick = () => {
    history.push(`/update/${adId}`);
  };

  const handleChatClick = () => {
    history.push(`/chat`);
  };

  const renderUserButtons = () => {
    if (adDetails.isAdBelongToCurrentUser) {
      return (
        <div>
          <button type="button" className="btn btn-success inline-button" onClick={() => handleUpdateClick()}>
            Update ad
          </button>
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => handleDeleteClick()}
            value={adId}
          >
            Delete ad
          </button >
        </div>
      )
    }
    else {
      return(
        <div>
          <button type="button" className="btn btn-primary" onClick={() => handleChatClick()}>
            Chat with seller
          </button>
        </div>
      )
    }
  };

  return (
    <div className="container">
      <div className="card">
        <div className="card-body">
          <h3 className="card-title">{adDetails.title}</h3>
          <h6 className="card-subtitle">Сategory: {adDetails.categoryName}</h6>
          <div className="row">
            <div className="col-lg-5 col-md-5 col-sm-6">
              <div className="white-box text-center">
                <img
                  src={adDetails.imageBase64}
                  alt=""
                  className="img-responsive"
                />
              </div>
            </div>
            <div className="col-lg-7 col-md-7 col-sm-6">
              <h4 className="box-title mt-5">Product description:</h4>
              <p>{adDetails.description}</p>
              <h2 className="mt-5">${adDetails.price}</h2>
              <h4 className="box-title mt-7">
                Location: {adDetails.goodsLocation}
              </h4>
              <h4 className="box-title mt-7">
                Seller name: {adDetails.sellerName}
              </h4>
              <h4 className="box-title mt-7">
                Сontact number: {adDetails.contactNumber}
              </h4>
              {renderUserButtons()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdDetails;
