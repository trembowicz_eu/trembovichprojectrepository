import React from 'react';
import Layout from '../../layout';
import  {BaseUri} from '../../config.json'
import Filter from './filter';
import Board from './board';
import LoaderImage from '../../images/loader.svg'

class BoardPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      ads: [],
      categories: [],
      isLoading: false,
      categoryId: 0,
      numberOfAds: 0,
      currentPage: 1
    };
  }
  
  componentDidMount() {
    this.setState({isLoading : true});
    
    fetch(`${BaseUri}/Ad/getAllAdsByDate`, { 
      headers: new Headers({
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      })
    })
    .then(result => result.json())
    .then(result => this.setState({
      ads: result.ads,
      numberOfAds: result.count,
      isLoading: false 
    }));

    fetch(`${BaseUri}/Category/get`, { 
      headers: new Headers({
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      })
    })
    .then(result => result.json())
    .then(result => this.setState({
      categories: result
    }));
  }

  handleCategoryChange = (categoryId) => {
    this.setState({isLoading : true,
    categoryId : categoryId});
    
    fetch(`${BaseUri}/Ad/getAllAdsByDate?categoryId=${categoryId}`, { 
      headers: new Headers({
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      })
    })
    .then(result => result.json())
    .then(result => this.setState({
      ads: result.ads,
      numberOfAds: result.count,
      isLoading: false 
    }))
  }

  handlePageClick = (currentPage) => {
    this.setState({isLoading : true,
    currentPage : currentPage});
    
    fetch(`${BaseUri}/Ad/getAllAdsByDate?categoryId=${this.state.categoryId}&pageNumber=${currentPage}`, { 
      headers: new Headers({
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      })
    })
    .then(result => result.json())
    .then(result => this.setState({
      ads: result.ads,
      numberOfAds: result.count,
      isLoading: false 
    }))
  }
  
  render() {
    window.scrollTo(0, 0);
    const {isLoading, categories, ads, categoryId, numberOfAds} = this.state
    return(
      <Layout>
        {
          !isLoading &&
            <>
              <Filter categories={categories} selectedCategoryId={categoryId} onChange={this.handleCategoryChange}/>
              <Board ads={ads} numberOfAds={numberOfAds} onClick={this.handlePageClick} currentPage={this.state.currentPage}/>
            </>
        }
        {
          isLoading && 
            <div className="loader">
              <img src={LoaderImage} alt="..." />
            </div>
        }
      </Layout>
    )
  }
}

export default BoardPage