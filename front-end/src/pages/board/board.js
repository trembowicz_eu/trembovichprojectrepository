import React from 'react'
import { Link } from 'react-router-dom';

const Board = (props) => {
  const {currentPage} = props;
  const numberOfAdsPerPage = 9;

  const getPageClassName = (index) => {
    return index === parseInt(currentPage) ? "active" : "";
  }

  const handleClick = (number) => () => {
    props.onClick(number);
  };

  const renderAdsBoard = () => {
    return(
      props.ads.map((ad, index) => 
        <div key={index} className="col-lg-4 col-md-6 mb-4">
          <div className="card h-100">
          <img src={ad.imageBase64} alt="" />
            <div className="card-body">
              <h4 className="card-title">
                <Link to={`details/${ad.id}`}>{ad.title}</Link>
              </h4>
              <h5>${ad.price}</h5>
              <p className="card-text">Category: {ad.categoryName}</p>
              Location: {ad.goodsLocation}
            </div>
          </div>
        </div>
      )
    )
  }

  const renderPagination = () => {
    let numberOfPages;

    if(props.numberOfAds <= numberOfAdsPerPage ){
      numberOfPages = 1;
    }
    else{
      if(props.numberOfAds % numberOfAdsPerPage === 0){
        numberOfPages = Math.floor(props.numberOfAds/numberOfAdsPerPage);
      }
      else{
        numberOfPages = Math.floor(props.numberOfAds/numberOfAdsPerPage) + 1;
      }
    }

    const pageNumbers = [];
    for (var i = 1; i <= numberOfPages; i++) {
      pageNumbers.push(i);
    }

    return(
      pageNumbers.map(number => 
        <li key={number} className={"page-item " + getPageClassName(number)}>
          <button className="page-link" onClick={handleClick(number)} value={number}>
            {number}
          </button>
        </li>
      )
    )
  }

  return(
    <div className="col-lg-9">
      <h1 className="my-4 text-center">Ads</h1>
      <div className="row">
        {renderAdsBoard()}
      </div>
      <nav>
        <ul className="pagination justify-content-center">
          {renderPagination()}
        </ul>
      </nav>
    </div>
  )
}

export default Board
