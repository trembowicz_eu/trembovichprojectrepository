import React from 'react'
import services from '../../services' 
import CategoryDropDown from './category-dropdown';
import { useHistory } from 'react-router-dom';

const UpdateForm = (props) => {
  const { adFieldsToUpdate } = props;
  const {adId} = props;
  const history = useHistory()
  const [title, setTitle] = React.useState(adFieldsToUpdate.title);
  const [description, setDescription] = React.useState(adFieldsToUpdate.description);
  const [category, setCategory] = React.useState(adFieldsToUpdate.categoryId);
  const [goodsLocation, setGoodsLocation] = React.useState(adFieldsToUpdate.goodsLocation);
  const [price, setPrice] = React.useState(adFieldsToUpdate.price);
  const [contactNumber, setContactNumber] = React.useState(adFieldsToUpdate.contactNumber);
  const [image, setImage] = React.useState('');
  const [imageBase64, setImageBase64] = React.useState(adFieldsToUpdate.imageBase64);
  const [imageType, setImageType] = React.useState();
  const [errors, setErrors] = React.useState({});
  const reader = new FileReader();
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    errors.Title = null;
    errors.Description = null;
    errors.Category = null;
    errors.GoodsLocation = null;
    errors.Price = null;
    errors.ContactNumber = null;
    errors.Image = null;
    errors.UpdateAdFailed = null;
    
    if (!isValid()){
      return;
    }
    
    let isUpdateAdSuccessfull = await services.UpdateService.updateAd(adId, title, description, category, goodsLocation, price, contactNumber, imageBase64);

    if(isUpdateAdSuccessfull){
      history.goBack();
    }
    else{
      let errors = {};
      errors.UpdateAdFailed = "Something was wrong!";
      setErrors(errors);
    }
  }

  const isValid = () => {
    let isValid = true;
    let errors = {};

    if(title.trim() === "") {
      errors.Title = "Title is required.";
      setErrors(errors);
      isValid = false;
    }

    if(description.trim() === "") {
      errors.Description = "Description is required.";
      setErrors(errors);
      isValid = false;
    }

    if(category === '0'){
      errors.Category = "Category is required.";
      setErrors(errors);
      isValid = false;
    }

    if(goodsLocation.trim() === "") {
      errors.GoodsLocation = "Goods location is required.";
      setErrors(errors);
      isValid = false;
    }

    var priceRegex = /^\d+(\.\d{0,2})?$/g;
    if(price === '0' || price === '0.' || price ==='0.0' || price === '0.00' || price === '') {
      errors.Price = "Price is required.";
      setErrors(errors);
      isValid = false;
    } else if (!priceRegex.test(price)) {
      errors.Price = "Invalid format.";
      setErrors(errors);
      isValid = false;
    } 

    var contactNumberRegex = /^\+[0-9]{12}$/;
    if(contactNumber.trim() === "") {
      errors.ContactNumber = "Contact number is required.";
      setErrors(errors);
      isValid = false;
    } else if (!contactNumberRegex.test(contactNumber)) {
      errors.ContactNumber = "Invalid format. Allowed format +xxxxxxxxxxxx";
      setErrors(errors);
      isValid = false;
    }

    if(image.trim() === "") {
      setImageBase64(adFieldsToUpdate.imageBase64);
    } else if (!(imageType === "image/jpeg")) {
      errors.Image = "Uploaded file is not a valid image. Only JPG files are allowed.";
      setErrors(errors);
      isValid = false;
    }
    return isValid;
  }

  const uploadImage = (e) => {
    if(e.target.files[0] === undefined){
      setImageType(null);
    }else{
      setImageType(e.target.files[0].type)
    }
    setImage(e.target.value);
    reader.onload = function () {
      setImageBase64(reader.result);
    }; 
    reader.onerror = function (error) { 
      console.log('Error: ', error); 
    };

    if(e.target.files[0] === undefined){
      reader.readAsDataURL(new Blob());
    }else{
      reader.readAsDataURL(e.target.files[0]);
    }
  }

return(
  <div>
    <div className="col-lg-6">
      <h4>Update form</h4>
        <form onSubmit={handleFormSubmit}>
          <div className="form-group">
            <label>Title:</label>
            <input type="text" name="title" onChange={e=>setTitle(e.target.value)} value={title} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Title}</span>
          </div>
          <div className="form-group">
            <label>Description:</label>
            <input type="text" name="description" onChange={e=>setDescription(e.target.value)} value={description} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Description}</span>
          </div>
          <div className="form-group">
            <CategoryDropDown categoryId={adFieldsToUpdate.categoryId} onChange={e=>setCategory(e)} value={category}/>
            <span style={{ color: "red" }}>{errors.Category}</span>
          </div>
          <div className="form-group">
            <label>Goods location:</label>
            <input type="text" name="goodsLocation" onChange={e=>setGoodsLocation(e.target.value)} value={goodsLocation} className="form-control"/>
            <span style={{ color: "red" }}>{errors.GoodsLocation}</span>
          </div>
          <div className="form-group">
            <label>Price:</label>
            <input type="text" name="price" onChange={e=>setPrice(e.target.value)} placeholder="0.00" step="0.01" min="0.00" value={price} className="form-control"/>
            <span style={{ color: "red" }}>{errors.Price}</span>
          </div>
          <div className="form-group">
            <label>Contact number:</label>
            <input type="tel" name="contactNumber" onChange={e=>setContactNumber(e.target.value)} value={contactNumber} className="form-control"/>
            <span style={{ color: "red" }}>{errors.ContactNumber}</span>
          </div>
          <div className="form-group">
          <div className="col-lg-5 col-md-5 col-sm-6">
              <div className="white-box text-center">
                <img
                  src={adFieldsToUpdate.imageBase64}
                  alt=""
                  className="img-responsive"
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label>New photo:</label>
            <input type="file" name="imag" onChange={uploadImage} value={image} className="form-control" />
            <span style={{ color: "red" }}>{errors.Image}</span>
            <p className='text-danger'>{errors.UpdateAdFailed}</p>
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary btn-lg btn-block">Update</button>
          </div>
        </form>
    </div>
  </div>
  )
}

export default UpdateForm