import React from "react";
import Layout from "../../layout";
import UpdateForm from "./form";
import { BaseUri } from "../../config.json";
import LoaderImage from "../../images/loader.svg";


class UpdatePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      adFieldsToUpdate: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    fetch(`${BaseUri}/Ad/getAdFieldsToUpdate?adId=${this.props.id}`, {
      headers: new Headers({
        Authorization: "Bearer " + window.localStorage.getItem("token"),
      }),
    })
      .then((result) => result.json())
      .then((result) =>
        this.setState({
          adFieldsToUpdate: result,
          isLoading: false,
        })
      );
  }

  renderUpdateFormOrAcceesMessаge = () => {
    if (this.state.adFieldsToUpdate.isAdBelongToCurrentUser) {
      return (
        <>
          <UpdateForm adFieldsToUpdate={this.state.adFieldsToUpdate} adId={this.props.id} />
        </>
      );
    } else{
      return (
        <Layout>
          <div>
            <h5>
              You don't have access
            </h5>
          </div>
        </Layout>
      );
    }
  };

  render() {
    const { isLoading } = this.state;
        return (
          <Layout>
            {!isLoading && (
              this.renderUpdateFormOrAcceesMessаge()
            )}
            {isLoading && (
              <div className="loader">
                <img src={LoaderImage} alt="..." />
              </div>
            )}
          </Layout>
        );
    }
  }

export default UpdatePage;
