import React from "react";
import PropTypes from "prop-types";
import { BaseUri } from "../../config.json";

class CategoryDropDown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      selctedCategoryId: 0,
    };
    this._isMounted = false;
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    fetch(`${BaseUri}/Category/get`, {
      headers: new Headers({
        Authorization: "Bearer " + window.localStorage.getItem("token"),
      }),
    })
      .then((result) => result.json())
      .then((result) => {
        if (this._isMounted) {
          this.setState({
            categories: result,
            selctedCategoryId: this.props.categoryId,
          });
        }
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChange(e) {
    this.setState({ selctedCategoryId: e.target.value });
    this.props.onChange(e.target.value);
  }

  render() {
    return (
      <div>
        <label>Category:</label>
        <select
          className="form-control"
          value={this.state.selctedCategoryId}
          onChange={this.handleChange}
        >
          <option value="0">Choose category</option>
          {this.state.categories.map((category) => (
            <option key={category.id} value={category.id}>
              {category.name}
            </option>
          ))}
        </select>
      </div>
    );
  }
}

CategoryDropDown.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default CategoryDropDown;
