import React from "react";
import Layout from "../../layout";
import Chat from "./chat";

const ChatPage = () => {
  return (
    <Layout>
      <Chat/>
    </Layout>
  );
}
export default ChatPage;