import config from '../config.json';
import LoginService from './loginService';
import SignupServise from './signUpService';
import AdServise from './adService';
import UpdateServise from './updateService';


const baseUri = config.BaseUri;
const services = {
  LoginService : new LoginService(baseUri),
  SignupServise : new SignupServise(baseUri),
  AdServise : new AdServise(baseUri),
  UpdateService : new UpdateServise(baseUri)
}

export default services;