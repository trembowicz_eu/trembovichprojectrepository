class LoginServise {
  constructor(baseUri){
    this.BaseUri = baseUri; 
  }

  async login(userName, password){
    const response = await fetch(`${this.BaseUri}/authenticate/login`, {
      method: 'POST',
      headers: {
        "Content-Type" : "application/json"
      },
      body: JSON.stringify({userName, password})
    });
    const data = await response.json();
    window.localStorage.setItem("token", data.token);
    window.localStorage.setItem("expiration", data.expiration);
    if(response.status === 200){
      window.localStorage.setItem("isUserLogged", true);
      return true;
    }
    else{
      window.localStorage.setItem("isUserLogged", false);
      return false;
    }
  }
}

export default LoginServise;