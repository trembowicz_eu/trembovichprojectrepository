class UpdateServise {
  constructor(baseUri){
    this.BaseUri = baseUri; 
  }

  async updateAd(adId, title, description, categoryId, goodsLocation, price, contactNumber, imageBase64){
    const response = await fetch(`${this.BaseUri}/ad/update?adId=${adId}`, {
      method: 'POST',
      headers: {
        "Content-Type" : "application/json",
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      },
      body: JSON.stringify({title, description, categoryId, goodsLocation, price, contactNumber, imageBase64})
    });
    if(response.status === 200){
      return true;
    }
    else{
      return false;
    }
  }
}

export default UpdateServise;