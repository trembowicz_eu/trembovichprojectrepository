class AdServise {
  constructor(baseUri){
    this.BaseUri = baseUri; 
  }

  async createAd(title, description, categoryId, goodsLocation, price, contactNumber, imageBase64){
    const response = await fetch(`${this.BaseUri}/ad/create`, {
      method: 'POST',
      headers: {
        "Content-Type" : "application/json",
        'Authorization': 'Bearer ' + window.localStorage.getItem("token")
      },
      body: JSON.stringify({title, description, categoryId, goodsLocation, price, contactNumber, imageBase64})
    });
    if(response.status === 200){
      return true;
    }
    else{
      return false;
    }
  }
}

export default AdServise;