import './App.css';
import Routes from './common/routes/routes'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle.js'

function App() {
  return (
    <Routes />
  );
}

export default App;
