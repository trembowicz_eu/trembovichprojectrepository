import React from 'react'
import PropTypes from 'prop-types'

import Header from './header'
import Footer from './footer'

class Layout extends React.Component {
  render() {
    const { children } = this.props;
    return(
      <div className="page-container">
        <Header />
        <div className="container content-wrap">
          <div className="row">
              {children}
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout