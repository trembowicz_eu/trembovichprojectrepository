import React from 'react'
import { Link } from 'react-router-dom'
import  {Routes} from '../config.json'

class Header extends React.Component {



  logout() {
    window.localStorage.setItem('token', '');
    window.localStorage.setItem('isUserLogged', false);
  }

  renderNavigation() {
    const isUserLogged = window.localStorage.getItem("isUserLogged");
    if(JSON.parse(isUserLogged)) {
      return(
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link className="nav-link" to={Routes.Board}>All ads</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={Routes.OwnBoard}>My ads</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={Routes.Login} onClick={this.logout}>Logout</Link>
            </li>
          </ul>
        </div>
      );
    }
  }

  render() {
    return(
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div className="container">
              <Link className="navbar-brand" to="{Routes.Login}" onClick={this.logout} >Online market</Link>
              {this.renderNavigation()}
            </div>
          </nav>
    );
  }
}

export default Header