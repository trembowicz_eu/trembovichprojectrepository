import React from 'react'

class Footer extends React.Component {
  getYear() {
    return new Date().getFullYear();
  }

  render() {
    return(
      <footer className="py-3 bg-dark footer">
        <div className="container">
          <p className="m-0 text-center text-white">Copyright &copy; OnlineMarket 2021-{this.getYear()}</p>
        </div>
      </footer>
    )
  }
}

export default Footer