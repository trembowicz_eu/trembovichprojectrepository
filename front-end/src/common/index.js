const Constants = {
    URL_LOGIN: '/login',
    URL_SIGN_UP: '/signup',
    URL_BOARD: '/board',
    URL_AD: '/ad'
  }

const ErrorCodes = {
  UsernameAlreadyExists: "1"
}

export { Constants, ErrorCodes };