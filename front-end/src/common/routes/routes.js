import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import  {Routes as RoutsConfig} from '../../config.json'

//import { Constants } from '../index';

import LoginPage from '../../pages/login'
import SignUp from '../../pages/sign-up'
import Board from '../../pages/board'
import Ad from '../../pages/ad'
import AdDetailsPage from '../../pages/adDetails'
import OwnBoard from '../../pages/ownBoard'
import UpdatePage from '../../pages/updateForm'
import ChatPage from '../../pages/chat'

class Routes extends React.Component {
  render() {
    return(
        <Router>
          <Switch>
            <Route path={RoutsConfig.Login} component={LoginPage} />
            <Route path={RoutsConfig.SignUp} component={SignUp} />
            <Route path={RoutsConfig.Board} component={Board} />
            <Route path={RoutsConfig.Ad} component={Ad}/>
            <Route path={RoutsConfig.OwnBoard} component={OwnBoard} />
            <Route path={RoutsConfig.Chat} component={ChatPage} />
            <Route
              exact
              path={RoutsConfig.AdDetails}
              render={({ match }) => (
                <AdDetailsPage id={match.params.id} />
              )}
            />
            <Route
              exact
              path={RoutsConfig.Update}
              render={({ match }) => (
                <UpdatePage id={match.params.id} />
              )}
            />
            <Redirect to={RoutsConfig.Login} />
          </Switch>
        </Router>
    )
  }
}

export default Routes