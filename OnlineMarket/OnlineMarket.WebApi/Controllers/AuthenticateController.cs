﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineMarket.BLL.DTO;
using OnlineMarket.BLL.DTO.Responses;
using OnlineMarket.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineMarket.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthenticateController : ControllerBase
    {
        private readonly IAuthentication authientificationService;

        public AuthenticateController(IAuthentication authientificationService)
        {
            this.authientificationService = authientificationService;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var result = await authientificationService.Login(model);
            
            if (result != null)
                return Ok(result);

            return Unauthorized();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            /*var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userid = identity.Claims.First(x => x.Type == "userid");

            }*/
            var result = await authientificationService.Register(model);

            if (result.Succeeded)            
                return Ok();

            if (result.Errors != null)
            {
                return StatusCode(500);
            }
            
            return BadRequest();


            /*var response = new ResponseDto { Success = true, ErrorCode = ErrorCode.UsernameAlreadyExists };

            return Ok(response);*/
            //}

            /*if (result.Errors == new Id
                return Ok();*/


            /*var errors = await authientificationValidationService.Validate(model);

            if (errors.Any())
            {
                var response = new ResponseDto { Success = false, ErrorCode = ErrorCode.UsernameAlreadyExists };

                return Ok(response);
            }

            var response = await authientificationService.Register(model);
            return Ok(response);*/
        }
    }
}