﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using OnlineMarket.BLL.Hubs;
using OnlineMarket.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineMarket.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class ChatController : ControllerBase
    {
        private readonly IHubContext<ChatHub> _chatHub;
        public ChatController(IHubContext<ChatHub> chatHub)
        {
            _chatHub = chatHub;
        }

        /*[HttpPost("messages")]
        public async Task SendPrivateMessage(string userId, ChatMessage message)
        {
            
            
            await _chatHub.Clients.User(userId).SendAsync("ReceiveMessage", message);
        }*/

        [HttpPost("messages")]
        public async Task SendMessage(ChatMessage message)
        {
            var id = string.Empty;

            await _chatHub.Clients.All.SendAsync(id, message);
        }

    }   
}
