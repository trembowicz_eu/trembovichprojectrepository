﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineMarket.BLL.DTO.Ad;
using OnlineMarket.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineMarket.WebApi.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class AdController : ControllerBase
    {
        private readonly IAdService _adService;

        public AdController(IAdService adService)
        {
            _adService = adService;
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateAdAsync([FromBody] AdDto model)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userId = identity.Claims.First(x => x.Type == "userid").Value.ToString();
                await _adService.CreateAdAsync(model, userId);
            }

            return Ok();
        }

        [HttpGet]
        [Route("getAllAdsByDate")]
        public async Task<BoardDto> GetAllAds(int? categoryId, int pageNumber = 1)
        {
            return await _adService.GetAllAds(categoryId, pageNumber);
        }

        [HttpGet]
        [Route("getAdById")]
        public async Task<DetailsAdDto> GetAdById(int adId)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            var userId = identity.Claims.First(x => x.Type == "userid").Value.ToString();

            return await _adService.GetAdById(adId, userId);
        }

        [HttpGet]
        [Route("getUserAds")]
        public async Task<BoardDto> GetUserAds(int? categoryId, int pageNumber = 1)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            var userId = identity.Claims.First(x => x.Type == "userid").Value.ToString();

            return await _adService.GetUserAds(userId, categoryId, pageNumber);
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<IActionResult> DeleteAdAsync(int adId)
        {
            await _adService.DeleteAdAsync(adId);

            return Ok();
        }

        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateAdAsync(int adId, [FromBody] AdDto newModel)
        {
            await _adService.UpdateAdAsync(adId, newModel);

            return Ok();
        }

        [HttpGet]
        [Route("getAdFieldsToUpdate")]
        public async Task<UpdateAdDto> GetAdFieldsToUpdate(int adId)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            var userId = identity.Claims.First(x => x.Type == "userid").Value.ToString();

            return await _adService.GetAdFieldsToUpdate(adId, userId);
        }
    }
}
