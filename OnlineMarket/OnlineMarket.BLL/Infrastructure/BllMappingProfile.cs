﻿using AutoMapper;
using OnlineMarket.BLL.DTO;
using OnlineMarket.BLL.DTO.Ad;
using OnlineMarket.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Infrastructure
{
    public class BllMappingProfile : Profile
    {
        public BllMappingProfile()
        {
            CreateMap<CategoryDto, Category>().ReverseMap();

            CreateMap<AdDto, Ad>().ReverseMap();

            CreateMap<UpdateAdDto, Ad>().ReverseMap();

            CreateMap<Ad, GetAdDto>()
                .ForMember(x => x.CategoryName, x => x.MapFrom(y => y.Category.Name));

            CreateMap<Ad, DetailsAdDto>()
                .ForMember(x => x.CategoryName, x => x.MapFrom(y => y.Category.Name))
                .ForMember(x => x.SellerName, x => x.MapFrom(y => y.Seller.UserName));
        }
    }
}
