﻿using Microsoft.AspNetCore.SignalR;
using OnlineMarket.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Hubs
{
    public class ChatHub : Hub
    {
        /*public override Task OnConnectedAsync()
        {
            Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name);
            return base.OnConnectedAsync();
        }*/

        /*public async Task SendMessage(string sender, string receiver, ChatMessage message)
        {
            await Clients.Group(receiver).SendAsync("ReceiveMessage", sender, message);
        }*/
        /*public async Task SendMessage(ChatMessage message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }*/

        public async Task SendPrivateMessage(string userId, string message)
        {
            await Clients.User(userId).SendAsync("ReceiveMessage", message);
        }
    }
}
