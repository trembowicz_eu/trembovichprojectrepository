﻿using AutoMapper;
using OnlineMarket.BLL.DTO.Ad;
using OnlineMarket.BLL.Interfaces;
using OnlineMarket.DAL.Entities;
using OnlineMarket.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Services
{
    public class AdService : IAdService
    {
        private readonly IAdRepository _adRepository;

        private readonly IMapper _mapper;

        public AdService(IAdRepository adRepository, IMapper mapper)
        {
            _adRepository = adRepository;
            _mapper = mapper;
        }

        public async Task CreateAdAsync(AdDto model, string userId)
        {
            var result = Regex.Replace(model.ImageBase64, @"^data:image\/[a-zA-Z]+;base64,", string.Empty);

            var imageBytes = Convert.FromBase64String(result);

            var dbAd = _mapper.Map<Ad>(model);

            dbAd.Image = imageBytes;
            dbAd.SellerId = userId;
            dbAd.SubmissionDate = DateTime.Now;

            await _adRepository.CreateAsync(dbAd);
        }

        public async Task<DetailsAdDto> GetAdById(int adId, string userId)
        {
            var jpgImageRegex = "data:image/jpeg;base64,";

            var dbAdById = await _adRepository.GetAdById(adId);

            var dtoAdById = _mapper.Map<DetailsAdDto>(dbAdById);

            if(dbAdById.Image == null)
            {
                dtoAdById.ImageBase64 = null;
            }
            else
            {
                dtoAdById.ImageBase64 = jpgImageRegex + Convert.ToBase64String(dbAdById.Image);
            }

            if (dbAdById.SellerId == userId)
            {
                dtoAdById.IsAdBelongToCurrentUser = true;
            }
            else
            {
                dtoAdById.IsAdBelongToCurrentUser = false;
            }

            return dtoAdById;
        }

        public async Task<BoardDto> GetAllAds(int? categoryId, int pageNumber)
        {
            var jpgImageRegex = "data:image/jpeg;base64,";
            var pageSize = 9;
            var allDbAds = await _adRepository.GetAllAds();

            IEnumerable<Ad> filteredDbAds;
            IEnumerable<GetAdDto> dtoAds;
            IEnumerable<Ad> dbAdsPerPage;

            if (categoryId == null || categoryId == 0)
            {
                filteredDbAds = allDbAds;
            }
            else
            {
                filteredDbAds = allDbAds.Where(a => a.CategoryId == categoryId);
            }

            dbAdsPerPage = filteredDbAds.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            dtoAds = _mapper.Map<IEnumerable<GetAdDto>>(dbAdsPerPage);

            for (int i = 0; i < dtoAds.Count(); i++)
            {
                if (dbAdsPerPage.ElementAt(i).Image == null)
                {
                    dtoAds.ElementAt(i).ImageBase64 = null;
                }
                else
                {
                    dtoAds.ElementAt(i).ImageBase64 = jpgImageRegex + Convert.ToBase64String(dbAdsPerPage.ElementAt(i).Image);
                }
            }

            return new BoardDto { Ads = dtoAds, Count = filteredDbAds.Count() };
        }

        public async Task<BoardDto> GetUserAds(string userId, int? categoryId, int pageNumber)
        {
            var jpgImageRegex = "data:image/jpeg;base64,";
            var pageSize = 9;
            var allDbUserAds = await _adRepository.GetUserAds(userId);

            IEnumerable<Ad> filteredDbUserAds;
            IEnumerable<GetAdDto> dtoUserAds;
            IEnumerable<Ad> dbUserAdsPerPage;

            if (categoryId == null || categoryId == 0)
            {
                filteredDbUserAds = allDbUserAds;
            }
            else
            {
                filteredDbUserAds = allDbUserAds.Where(a => a.CategoryId == categoryId);
            }

            dbUserAdsPerPage = filteredDbUserAds.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            dtoUserAds = _mapper.Map<IEnumerable<GetAdDto>>(dbUserAdsPerPage);

            for (int i = 0; i < dtoUserAds.Count(); i++)
            {
                if (dbUserAdsPerPage.ElementAt(i).Image == null)
                {
                    dtoUserAds.ElementAt(i).ImageBase64 = null;
                }
                else
                {
                    dtoUserAds.ElementAt(i).ImageBase64 = jpgImageRegex + Convert.ToBase64String(dbUserAdsPerPage.ElementAt(i).Image);
                }
            }

            return new BoardDto { Ads = dtoUserAds, Count = filteredDbUserAds.Count() };
        }

        public async Task DeleteAdAsync(int adId)
        {
            await _adRepository.DeleteAsync(adId);
        }

        public async Task UpdateAdAsync(int adId, AdDto newAdDto)
        {
            var result = Regex.Replace(newAdDto.ImageBase64, @"^data:image\/[a-zA-Z]+;base64,", string.Empty);

            var imageBytes = Convert.FromBase64String(result);

            var dbAd = await _adRepository.GetItem(adId);

            dbAd.Title = newAdDto.Title;
            dbAd.Description = newAdDto.Description;
            dbAd.CategoryId = newAdDto.CategoryId;
            dbAd.GoodsLocation = newAdDto.GoodsLocation;
            dbAd.Price = newAdDto.Price;
            dbAd.ContactNumber = newAdDto.ContactNumber;
            dbAd.Image = imageBytes;

            await _adRepository.Save(dbAd);
        }

        public async Task<UpdateAdDto> GetAdFieldsToUpdate(int adId, string userId)
        {
            var jpgImageRegex = "data:image/jpeg;base64,";

            var dbAdById = await _adRepository.GetAdById(adId);

            var dtoAdById = _mapper.Map<UpdateAdDto>(dbAdById);

            if (dbAdById.Image == null)
            {
                dtoAdById.ImageBase64 = null;
            }
            else
            {
                dtoAdById.ImageBase64 = jpgImageRegex + Convert.ToBase64String(dbAdById.Image);
            }

            if (dbAdById.SellerId == userId)
            {
                dtoAdById.IsAdBelongToCurrentUser = true;
                return dtoAdById;
            }
            else
            {
                return new UpdateAdDto { IsAdBelongToCurrentUser = false };
            }
        }
    }
}
