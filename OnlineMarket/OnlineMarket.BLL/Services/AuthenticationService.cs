﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OnlineMarket.BLL.Constants;
using OnlineMarket.BLL.DTO;
using OnlineMarket.BLL.Interfaces;
using OnlineMarket.DAL.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Services
{
    public class AuthenticationService : IAuthentication
    {   
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;

        public AuthenticationService(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;
        }

        public async Task<object> Login(LoginModel model)
        {
            var user = await userManager.FindByNameAsync(model.Username);
            var passwordIsValid = await userManager.CheckPasswordAsync(user, model.Password);

            if (user != null && passwordIsValid)
            {
                var userRoles = await userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("userid", user.Id)
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                };
            }
            return null;
        }

        public async Task<IdentityResult> Register(RegisterModel model)
        {
            var userExists = await userManager.FindByNameAsync(model.Username);
            if (userExists != null)
                //return new ResponseDto { Success = false, ErrorCode = ErrorCode.UsernameAlreadyExists };
            return new IdentityResult();

            /*var emailExists = await userManager.FindByEmailAsync(model.Username);
            if (emailExists != null)
                return new ResponseDto { Success = false, ErrorCode = ErrorCode.EmailExists };*/

                ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username
            };

            return await userManager.CreateAsync(user, model.Password);
            ///return new ResponseDto { Success = true};
        }

        //public void RegisterAdmin(RegisterModel model)
        //{
        //    var userExists = userManager.FindByNameAsync(model.Username);
        //    if (userExists != null)
        //        return;

        //    ApplicationUser user = new ApplicationUser()
        //    {
        //        Email = model.Email,
        //        SecurityStamp = Guid.NewGuid().ToString(),
        //        UserName = model.Username
        //    };

        //    userManager.CreateAsync(user, model.Password);

        //    if (!roleManager.RoleExistsAsync(UserRoles.Admin))
        //        roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
        //    if (!roleManager.RoleExistsAsync(UserRoles.User))
        //        roleManager.CreateAsync(new IdentityRole(UserRoles.User));

        //    if (roleManager.RoleExistsAsync(UserRoles.Admin))
        //    {
        //        userManager.AddToRoleAsync(user, UserRoles.Admin);
        //    }
        //}
    }
}
