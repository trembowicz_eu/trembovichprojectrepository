﻿using AutoMapper;
using OnlineMarket.BLL.DTO;
using OnlineMarket.BLL.Interfaces;
using OnlineMarket.DAL.Entities;
using OnlineMarket.DAL.Interfaces;
using OnlineMarket.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> _categoryRepository;

        private readonly IMapper _mapper;

        public CategoryService(IRepository<Category> categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<List<CategoryDto>> GetCategoriesAlphabeticallyAsync()
        {
            var categoriesDb = await _categoryRepository.GetAllItemsAsync();

            var categoryDtoList = _mapper.Map<List<CategoryDto>>(categoriesDb);

            return categoryDtoList.OrderBy(x => x.Name).ToList();
        }
    }
}
