﻿using OnlineMarket.BLL.DTO.Ad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Interfaces
{
    public interface IAdService
    {
        Task CreateAdAsync(AdDto model, string userId);

        Task<BoardDto> GetAllAds(int? categoryId, int pageNumber);

        Task<DetailsAdDto> GetAdById(int adId, string userId);

        Task<BoardDto> GetUserAds(string userId, int? categoryId, int pageNumber);

        Task DeleteAdAsync(int adId);

        Task UpdateAdAsync(int adId, AdDto newAdDto);

        Task<UpdateAdDto> GetAdFieldsToUpdate(int adId, string userId);
    }
}
