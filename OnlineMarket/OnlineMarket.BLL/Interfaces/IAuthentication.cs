﻿using Microsoft.AspNetCore.Identity;
using OnlineMarket.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.Interfaces
{
    public interface IAuthentication
    {
        Task<object> Login(LoginModel model);

        Task<IdentityResult> Register(RegisterModel model);

        //void RegisterAdmin(RegisterModel model);
    }
}
