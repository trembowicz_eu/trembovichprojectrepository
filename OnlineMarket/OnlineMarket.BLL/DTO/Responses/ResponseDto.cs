﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.DTO.Responses
{
    public class ResponseDto
    {
        public bool Success { get; set; }

        public string ErrorMessage { get; set; }

        public ErrorCode ErrorCode { get; set; }
    }
}
