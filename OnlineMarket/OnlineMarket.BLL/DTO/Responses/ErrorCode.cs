﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.DTO.Responses
{
    public enum ErrorCode
    {
        UsernameAlreadyExists = 1
    }
}
