﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.DTO.Ad
{
    public class GetAdDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string CategoryName { get; set; }

        public string GoodsLocation { get; set; }

        public decimal Price { get; set; }

        public string ImageBase64 { get; set; }
    }
}
