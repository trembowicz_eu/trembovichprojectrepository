﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.DTO.Ad
{
    public class BoardDto
    {
        public IEnumerable<GetAdDto> Ads { get; set; }

        public int Count { get; set; }
    }
}
