﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.DTO.Ad
{
    public class AdDto
    {
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Range(1, int.MaxValue)]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "GoodsLocation is required")]
        public string GoodsLocation { get; set; }

        [Range(0.01, double.MaxValue)]
        //[RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Price  must be in the following format: 0.00")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "ContactNumber is required")]
        [RegularExpression(@"^(\+[0-9]{12})$", ErrorMessage = "The contact number must be in the following format: +xxxxxxxxxxxx")]
        //@"^\+375(\s+)?-?(25|29|33|44)-?(\s+)?[0-9]{7}$"
        public string ContactNumber { get; set; }

        [Required]
        public string ImageBase64 { get; set; }
    }
}
