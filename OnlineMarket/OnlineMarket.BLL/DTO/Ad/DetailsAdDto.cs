﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.BLL.DTO.Ad
{
    public class DetailsAdDto
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string CategoryName { get; set; }

        public string GoodsLocation { get; set; }

        public decimal Price { get; set; }

        public string ContactNumber { get; set; }

        public string ImageBase64 { get; set; }

        public string SellerName { get; set; }

        public bool IsAdBelongToCurrentUser { get; set; }
    }
}
