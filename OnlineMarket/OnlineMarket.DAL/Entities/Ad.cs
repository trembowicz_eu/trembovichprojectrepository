﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.DAL.Entities
{
    public class Ad : BaseEntity
    {
        public string SellerId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public DateTime SubmissionDate { get; set; }

        public string GoodsLocation { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public string ContactNumber { get; set; }

        public byte[] Image { get; set; }

        public virtual Category Category { get; set; }

        public virtual ApplicationUser Seller { get; set; }
    }
}
