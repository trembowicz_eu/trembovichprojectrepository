﻿using OnlineMarket.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.DAL.Interfaces
{
    public interface IAdRepository : IRepository<Ad>
    {
        Task<IEnumerable<Ad>> GetAllAds();

        Task<Ad> GetAdById(int adId);

        Task<IEnumerable<Ad>> GetUserAds(string userId);

    }
}
