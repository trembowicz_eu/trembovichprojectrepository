﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.DAL.Interfaces
{
    public interface IRepository<T>
    {
        Task CreateAsync(T entity);

        Task DeleteAsync(int entityId);

        Task<T> GetItem(int entityId);

        Task<IEnumerable<T>> GetAllItemsAsync();

        Task Save(T entity);
    }
}
