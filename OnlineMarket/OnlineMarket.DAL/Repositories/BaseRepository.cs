﻿using Microsoft.EntityFrameworkCore;
using OnlineMarket.DAL.EF;
using OnlineMarket.DAL.Entities;
using OnlineMarket.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.DAL.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private DbContext _context;

        protected DbSet<T> _dbSet;

        public BaseRepository(ApplicationDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public async Task CreateAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int entityId)
        {
            var entity = _dbSet.SingleOrDefault(x => x.Id == entityId);

            if (entity == null)
            {
                return;
            }

            _dbSet.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllItemsAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<T> GetItem(int entityId)
        {
            return await _dbSet.SingleOrDefaultAsync(x => x.Id == entityId);
        }


        public async Task Save(T newEntity)
        {
            _context.Entry(newEntity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
