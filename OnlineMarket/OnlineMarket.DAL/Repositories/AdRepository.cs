﻿using Microsoft.EntityFrameworkCore;
using OnlineMarket.DAL.EF;
using OnlineMarket.DAL.Entities;
using OnlineMarket.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMarket.DAL.Repositories
{
    public class AdRepository : BaseRepository<Ad>, IAdRepository
    {
        public AdRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Ad> GetAdById(int adId)
        {
            return await _dbSet
                .Include(x => x.Category)
                .Include(x => x.Seller)
                .SingleOrDefaultAsync(x => x.Id == adId);
        }

        public async Task<IEnumerable<Ad>> GetAllAds()
        {
            return await _dbSet.Include(x => x.Category).ToListAsync();
        }

        public async Task<IEnumerable<Ad>> GetUserAds(string userId)
        {
            return await _dbSet
                .Include(x => x.Category)
                .Include(x => x.Seller)
                .Where(x => x.SellerId == userId)
                .ToListAsync();
        }
    }
}
